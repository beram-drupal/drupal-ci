<?php

declare(strict_types=1);

namespace Drupal\Tests\drupalci_core\Unit\Controller;

use Drupal\drupalci_core\Builder\PonyViewBuilder;
use Drupal\drupalci_core\Controller\PonyController;
use Drupal\Tests\UnitTestCase;

/**
 * Class PonyControllerTest.
 *
 * @package Drupal\Tests\drupalci_core\Unit\Controller
 */
class PonyControllerTest extends UnitTestCase {

  public function testListAction(): void {
    $ponyViewBuilder = $this->createMock(PonyViewBuilder::class);
    $ponyViewBuilder->expects($this->once())
      ->method('viewList')
      ->willReturn(['the render array']);

    $ponyController = new PonyController($ponyViewBuilder);
    $this->assertArrayEquals(
      ['the render array'],
      $ponyController->listAction()
    );
  }

}
