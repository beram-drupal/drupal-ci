<?php

declare(strict_types=1);

namespace Drupal\Tests\drupalci_core\Unit\Builder;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupalci_core\Builder\PonyViewBuilder;
use Drupal\drupalci_core\Query\PonyQuery;
use Drupal\node\NodeStorage;
use Drupal\node\NodeViewBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Class PonyViewBuilderTest.
 *
 * @package Drupal\Tests\drupalci_core\Unit\Builder
 */
class PonyViewBuilderTest extends UnitTestCase {

  public function testViewList(): void {
    $ponyQuery = $this->createMock(PonyQuery::class);
    $nodeStorage = $this->createMock(NodeStorage::class);
    $viewBuilder = $this->createMock(NodeViewBuilder::class);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);

    $entityTypeManager->expects($this->once())
      ->method('getStorage')
      ->with('node')
      ->willReturn($nodeStorage);

    $entityTypeManager->expects($this->once())
      ->method('getViewBuilder')
      ->with('node')
      ->willReturn($viewBuilder);

    $ponyQuery->expects($this->once())
      ->method('getAllIdsSortedByChangedDate')
      ->willReturn([1, 2, 3]);

    $nodeStorage->expects($this->once())
      ->method('loadMultiple')
      ->with([1, 2, 3])
      ->willReturn(['one pony', 'two pony', 'three pony']);

    $viewBuilder->expects($this->once())
      ->method('viewMultiple')
      ->with(['one pony', 'two pony', 'three pony'], 'teaser')
      ->willReturn(['the render array']);

    $ponyViewBuilder = new PonyViewBuilder($entityTypeManager, $ponyQuery);
    $this->assertArrayEquals(['the render array'], $ponyViewBuilder->viewList());
  }

}
