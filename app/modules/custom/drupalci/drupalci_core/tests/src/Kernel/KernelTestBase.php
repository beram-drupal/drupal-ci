<?php

declare(strict_types=1);

namespace Drupal\Tests\drupalci_core\Kernel;

use Drupal\KernelTests\KernelTestBase as BaseKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class KernelTestBase.
 *
 * @package Drupal\Tests\drupalci_core\Functional
 */
class KernelTestBase extends BaseKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    // Required modules to enable the drupalci_core module.
    'node',
    'user',
    'drupalci_core',
    // Required modules to import the config files.
    'system',
    'field',
    'menu_ui',
    'drupalci_tests',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    if ($xdebugIsEnabled = $GLOBALS['__DRUPALCI_XDEBUG_IS_ENABLED']) {
      // Temporary disable code coverage to speed things up.
      \xdebug_stop_code_coverage(0);
    }

    parent::setUp();

    $this->setUpSite();

    if ($xdebugIsEnabled) {
      // Restore code coverage with the options to detect unused and dead code.
      \xdebug_start_code_coverage(XDEBUG_CC_UNUSED | XDEBUG_CC_DEAD_CODE);
    }
  }

  /**
   * Set up useful schema, configurations for the site.
   */
  protected function setUpSite(): void {
    // Required.
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installSchema('node', ['node_access']);
    $this->installConfig('drupalci_tests');
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    if ($GLOBALS['__DRUPALCI_XDEBUG_IS_ENABLED']) {
      // Once we tearDown, code coverage is useless.
      \xdebug_stop_code_coverage(0);
    }

    parent::tearDown();
  }

  /**
   * Create a "pony".
   *
   * @param string $title
   *   The title.
   *
   * @return \Drupal\node\NodeInterface
   *   The "pony" node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the pony could not be saved.
   */
  protected function createPony(string $title): NodeInterface {
    return $this->createNode('pony', $title);
  }

  /**
   * Create a node.
   *
   * @param string $bundle
   *   The bundle.
   * @param string $title
   *   The title.
   *
   * @return \Drupal\node\NodeInterface
   *   The created node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If the node could not be saved.
   */
  protected function createNode(string $bundle, string $title): NodeInterface {
    $node = Node::create([
      'type' => $bundle,
      'title' => $title,
    ]);
    $node->save();

    return $node;
  }

}
