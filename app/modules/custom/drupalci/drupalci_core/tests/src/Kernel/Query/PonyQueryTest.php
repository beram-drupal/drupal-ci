<?php

declare(strict_types=1);

namespace Drupal\Tests\drupalci_core\Kernel\Query;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupalci_core\Query\PonyQuery;
use Drupal\Tests\drupalci_core\Kernel\KernelTestBase;

/**
 * Class PonyQueryTest.
 *
 * @package Drupal\Tests\drupalci_core\Kernel\Query
 */
class PonyQueryTest extends KernelTestBase {

  /**
   * The pony query.
   *
   * @var \Drupal\drupalci_core\Query\PonyQuery
   */
  private PonyQuery $ponyQuery;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->ponyQuery = new PonyQuery($this->entityTypeManager);
  }

  public function testGetAllIdsSortedByChangedDate(): void {
    $applejack = $this->createPony('Applejack');
    $applejack->set('changed', 1255277097)->save();
    $fluttershy = $this->createPony('Fluttershy');
    $fluttershy->set('changed', 1256277097)->save();
    $rarity = $this->createPony('Rarity');
    $rarity->set('changed', 1258277097)->save();

    $this->createNode('page', 'A page');

    $this->assertSame(
      [
        $rarity->id() => $rarity->id(),
        $fluttershy->id() => $fluttershy->id(),
        $applejack->id() => $applejack->id(),
      ],
      $this->ponyQuery->getAllIdsSortedByChangedDate()
    );

    $applejack->set('changed', 1355277097)->save();

    $this->assertSame(
      [
        $applejack->id() => $applejack->id(),
        $rarity->id() => $rarity->id(),
        $fluttershy->id() => $fluttershy->id(),
      ],
      $this->ponyQuery->getAllIdsSortedByChangedDate()
    );
  }

}
