<?php

declare(strict_types=1);

namespace Drupal\drupalci_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupalci_core\Builder\PonyViewBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PonyController.
 *
 * @package Drupal\drupalci_core\Controller
 */
final class PonyController extends ControllerBase {

  /**
   * The pony view builder.
   *
   * @var \Drupal\drupalci_core\Builder\PonyViewBuilder
   */
  private PonyViewBuilder $ponyViewBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(PonyViewBuilder $ponyViewBuilder) {
    $this->ponyViewBuilder = $ponyViewBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('drupalci_core.builder.view_pony')
    );
  }

  /**
   * List the ponies.
   *
   * @return array
   *   The render array that list the ponies.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function listAction(): array {
    return $this->ponyViewBuilder->viewList();
  }

}
