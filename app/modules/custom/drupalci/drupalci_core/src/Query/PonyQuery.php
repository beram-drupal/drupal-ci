<?php

declare(strict_types=1);

namespace Drupal\drupalci_core\Query;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class PonyQuery.
 *
 * @package Drupal\drupalci_core\Query
 */
class PonyQuery {

  private const TYPE = 'pony';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * PonyQuery constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Get all "Ponies" IDs sorted by changed date.
   *
   * @return int[]
   *   The ponies' IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllIdsSortedByChangedDate(): array {
    return $this->getNodeStorage()->getQuery()
      ->condition('type', self::TYPE)
      ->sort('changed', 'DESC')
      ->execute();
  }

  /**
   * Get the node storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface|\Drupal\node\NodeStorage
   *   The node storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getNodeStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('node');
  }

}
