<?php

declare(strict_types=1);

namespace Drupal\drupalci_core\Builder;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\drupalci_core\Query\PonyQuery;

/**
 * Class PonyViewBuilder.
 *
 * @package Drupal\drupalci_core\Builder
 */
class PonyViewBuilder {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The pony query.
   *
   * @var \Drupal\drupalci_core\Query\PonyQuery
   */
  private PonyQuery $ponyQuery;

  /**
   * PonyViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\drupalci_core\Query\PonyQuery $ponyQuery
   *   The pony query.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, PonyQuery $ponyQuery) {
    $this->entityTypeManager = $entityTypeManager;
    $this->ponyQuery = $ponyQuery;
  }

  /**
   * Build the list of all ponies.
   *
   * @return array
   *   The render array to list all ponies.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function viewList(): array {
    $ponies = $this->entityTypeManager->getStorage('node')->loadMultiple(
      $this->ponyQuery->getAllIdsSortedByChangedDate()
    );

    return $this->entityTypeManager->getViewBuilder('node')
      ->viewMultiple($ponies, 'teaser');
  }

}
