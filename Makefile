include .env

.DEFAULT_GOAL := help

# Colors
COLOR_END ?= \033[0m
COLOR_RED ?= \033[31m
COLOR_GREEN ?= \033[32m
COLOR_YELLOW ?= \033[33m
COLOR_CYAN ?= \033[36m
# Inverted, i.e. colored backgrounds
COLOR_IRED ?= \033[0;30;41m
COLOR_IGREEN ?= \033[0;30;42m
COLOR_IYELLOW ?= \033[0;30;43m
COLOR_ICYAN ?= \033[0;30;46m

USER_SHELL ?= www-data

################################################################################
##@ Miscellaneous
################################################################################

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: make ${COLOR_GREEN}[target]${COLOR_END}\n\nAvailable ${COLOR_GREEN}targets${COLOR_END} by ${COLOR_CYAN}category${COLOR_END}:\n"} \
		/^[a-zA-Z_-]+:.*?##/ { \
			printf "\t${COLOR_GREEN}%-10s${COLOR_END}\t%s\n", $$1, $$2 \
		} \
		/^##@/ { \
			printf "\n${COLOR_CYAN}%s${COLOR_END}\n", substr($$0, 5) \
		}' \
		$(MAKEFILE_LIST)

.PHONY: clean
clean: ## Clean your repository (delete unusefull files such as vendor, reports etc..)
	rm -rf app/core
	rm -rf app/modules/contrib
	rm -rf app/profiles/contrib
	rm -rf app/themes/contrib
	rm -rf app/.csslintrc
	rm -rf app/.eslintignore
	rm -rf app/.eslintrc.json
	rm -rf app/.ht.router.php
	rm -rf app/.htaccess
	rm -rf app/autoload.php
	rm -rf app/example.gitignore
	rm -rf app/index.php
	rm -rf app/INSTALL.txt
	rm -rf app/README.txt
	rm -rf app/robots.txt
	rm -rf app/update.php
	rm -rf app/web.config
	rm -rf .editorconfig
	rm -rf .gitattributes
	rm -rf bin
	rm -rf build
	rm -rf vendor
	rm -rf web

.PHONY: very-clean
very-clean: ## Clean everything: .env, docker container/images/volumes, drupal files (media etc..), drupal settings etc..
	${MAKE} docker-prune
	${MAKE} clean
	rm -f .env
	rm -rf app/sites/default/files
	rm -rf app/sites/default/services.yml
	rm -rf app/sites/default/settings.local.php
	rm -rf app/sites/default/settings.php

################################################################################
##@ Project
################################################################################

.PHONY: project-upgrade
project-upgrade: ## to upgrade the project
	@echo "$(COLOR_IGREEN)Run Composer...$(COLOR_END)"
	${MAKE} drupal-composer-install
	@echo "$(COLOR_IGREEN)Upgrade Drupal App...$(COLOR_END)"
	${MAKE} drupal-app-upgrade

.PHONY: project-rebuild
project-rebuild: ## to rebuild the project from scratch
	@echo "$(COLOR_IYELLOW)TODO$(COLOR_END)"
	${MAKE} docker-rebuild
	@echo "$(COLOR_IYELLOW)Wait for all container to start before doing something else...$(COLOR_END)"
	sleep 10
	@echo "$(COLOR_IGREEN)Run composer...$(COLOR_END)"
	${MAKE} drupal-composer-install
	@echo "$(COLOR_IGREEN)Start Drupal installation...$(COLOR_END)"
	${MAKE} drupal-install
	@echo "$(COLOR_IGREEN)Visit your project with ${PROJECT_BASE_URL} ;)$(COLOR_END)"

.PHONY: project-install
project-install: ## to install the project
	${MAKE} docker-start
	@echo "$(COLOR_IYELLOW)Wait for all container to start before doing something else...$(COLOR_END)"
	sleep 10
	@echo "$(COLOR_IGREEN)Run composer...$(COLOR_END)"
	${MAKE} drupal-composer-install
	@echo "$(COLOR_IGREEN)Start Drupal installation...$(COLOR_END)"
	${MAKE} drupal-install
	@echo "$(COLOR_IGREEN)Visit your project with ${PROJECT_BASE_URL} ;)$(COLOR_END)"

.PHONY: project-install-vanilla
project-install-vanilla: ## to install the project with a vanilla Drupal
	${MAKE} docker-start
	@echo "$(COLOR_IYELLOW)Wait for all container to start before doing something else...$(COLOR_END)"
	sleep 10
	@echo "$(COLOR_IGREEN)Run composer...$(COLOR_END)"
	${MAKE} drupal-composer-install
	@echo "$(COLOR_IGREEN)Start Drupal installation...$(COLOR_END)"
	${MAKE} drupal-install-vanilla
	@echo "$(COLOR_IGREEN)Visit your project with ${PROJECT_BASE_URL} ;)$(COLOR_END)"

################################################################################
##@ Docker
################################################################################

.PHONY: docker-start
docker-start: ## to create and start all the containers
	@echo "$(COLOR_IGREEN)Starting up containers for $(PROJECT_NAME)...$(COLOR_END)"
	docker-compose pull
	docker-compose up -d --remove-orphans

.PHONY: docker-stop
docker-stop: ## to stop all the containers
	@echo "$(COLOR_IGREEN)Stopping containers for $(PROJECT_NAME)...$(COLOR_END)"
	@docker-compose stop

.PHONY: docker-prune
docker-prune: ## to stop and remove containers, networks, images, and volumes
	@echo "$(COLOR_IGREEN)Removing containers for $(PROJECT_NAME)...$(COLOR_END)"
	@docker-compose down -v --rmi all

.PHONY: docker-rebuild
docker-rebuild: ## to clean and up all docker
	$(MAKE) docker-prune
	${MAKE} docker-start

.PHONY: docker-logs
docker-logs: ## to show logs from containers
	@docker-compose logs -ft $(filter-out $@,$(MAKECMDGOALS))

.PHONY: docker-ps
docker-ps: ## to show all the containers
	docker ps --filter name='$(PROJECT_NAME)*'
	docker-compose ps

.PHONY: docker-shell-php
docker-shell-php: ## to open a shell session in the php container
	docker-compose exec --user ${USER_SHELL} -e LINES=$$(tput lines) -e COLUMNS=$$(tput cols) php bash

################################################################################
##@ Composer
################################################################################

.PHONY: composer
composer: ## to execute composer command
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) composer $(cmd)

################################################################################
##@ Quality
################################################################################

.PHONY: quality-analysis
quality-analysis: ## to analyse quality of the project
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) ./bin/phpcs
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) ./bin/phpmd ./app/modules/custom ansi phpmd.xml.dist
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) ./bin/phpstan analyse
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) ./scripts/run-tests.sh
	docker run --rm -v "$(PWD):/mnt" koalaman/shellcheck:stable scripts/*.sh scripts/**/*.sh

################################################################################
##@ Drupal
################################################################################

.PHONY: drush
drush: ## to execute drush command
	docker exec --user ${USER_SHELL} $(shell docker-compose ps -q php) drush -r $(DRUPAL_ROOT_DIRECTORY) $(cmd)

.PHONY: drupal-cache-rebuild
drupal-cache-rebuild: ## to clear Drupal cache
	${MAKE} drush cmd='cr $(opt)'

.PHONY: drupal-backup-db
drupal-backup-db: ## to backup the Drupal DB
	${MAKE} drush cmd='sql:dump --gzip --result-file auto'

.PHONY: drupal-mode-dev
drupal-mode-dev: ## Enable the dev mode for a site.
	docker-compose exec --user ${USER_SHELL} php /bin/sh -c 'sh scripts/drupal-mode.sh -m dev'

.PHONY: drupal-mode-prod
drupal-mode-prod: ## Enable the prod mode for a site.
	docker-compose exec --user ${USER_SHELL} php /bin/sh -c 'sh scripts/drupal-mode.sh -m prod'

.PHONY: drupal-composer-install
drupal-composer-install: ## to execute composer commands for drupal
	${MAKE} composer cmd=install
	${MAKE} composer cmd=drupal:scaffold
	${MAKE} composer cmd=drupal:paranoia

.PHONY: drupal-install
drupal-install: ## to install the Drupal App.
	${MAKE} drush cmd="site:install ${DRUPAL_INSTALL_PROFILE} --db-url=${DB_DRIVER}://${DB_USER}:${DB_PASSWORD}@${DB_HOST}/${DB_NAME} --account-name=${DRUPAL_ADMIN_USER} --account-pass=${DRUPAL_ADMIN_PASSWORD} --account-mail=${DRUPAL_ADMIN_EMAIL} --site-name=${DRUPAL_SITE_NAME} --site-mail=${DRUPAL_ADMIN_EMAIL} -y"

.PHONY: drupal-app-upgrade
drupal-app-upgrade: ## to upgrade the Drupal app
	docker-compose exec --user ${USER_SHELL} php /bin/sh -c 'sh scripts/drupal-upgrade.sh default'
	@echo "$(COLOR_IGREEN)Upgrade complete$(COLOR_END)"
