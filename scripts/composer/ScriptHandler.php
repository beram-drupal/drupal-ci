<?php

/**
 * @file
 * Contains \DrupalProject\composer\ScriptHandler.
 */

namespace DrupalProject\composer;

use Composer\Script\Event;
use Composer\Semver\Comparator;
use Drupal\Core\Site\Settings;
use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Webmozart\PathUtil\Path;

class ScriptHandler {

  public static function createRequiredFiles(Event $event) {
    include_once getcwd() . '/load.environment.php';
    $fs = new Filesystem();
    $drupalFinder = new DrupalFinder();
    $drupalFinder->locateRoot(getcwd());
    $drupalRoot = $drupalFinder->getDrupalRoot();

    $dirs = [
      'modules',
      'profiles',
      'themes',
    ];

    // Required for unit testing
    foreach ($dirs as $dir) {
      if (!$fs->exists($drupalRoot . '/'. $dir)) {
        $fs->mkdir($drupalRoot . '/'. $dir);
        $fs->touch($drupalRoot . '/'. $dir . '/.gitkeep');
      }
    }

    // Prepare the settings file for installation
    if (!$fs->exists($drupalRoot . '/sites/default/settings.php') and $fs->exists($drupalRoot . '/sites/default/default.settings.php')) {
      $fs->copy($drupalRoot . '/sites/default/default.settings.php', $drupalRoot . '/sites/default/settings.php');
      require_once $drupalRoot . '/core/includes/bootstrap.inc';
      require_once $drupalRoot . '/core/includes/install.inc';
      new Settings([]);
      $settings['settings']['config_sync_directory'] = (object) [
        'value' => Path::makeRelative($drupalFinder->getComposerRoot() . '/config/sync', $drupalRoot),
        'required' => TRUE,
      ];
      $project_available_envs = explode(',', getenv('PROJECT_AVAILABLE_ENVS'));
      foreach ($project_available_envs as $project_available_env) {
        $settings['config']['config_split.config_split.' . $project_available_env] = [
          'status' => (object) [
            'value' => (getenv('PROJECT_ENV') === $project_available_env),
            'required' => TRUE,
          ],
        ];
      }
      drupal_rewrite_settings($settings, $drupalRoot . '/sites/default/settings.php');

      $fs->chmod($drupalRoot . '/sites/default/settings.php', 0775);
      $event->getIO()->write("Create a sites/default/settings.php file with chmod 0775");
    }

    // Create local settings file.
    if (getenv('PROJECT_ENV') === 'local'
      && !$fs->exists($drupalRoot . '/sites/default/settings.local.php')
      && $fs->exists($drupalRoot . '/sites/example.settings.local.php')) {
      $fs->copy(
        $drupalRoot . '/sites/example.settings.local.php',
        $drupalRoot . '/sites/default/settings.local.php'
      );
      $event->getIO()->write('Create a sites/default/settings.local.php file');
    }

    // Create services.yml file.
    if (!$fs->exists($drupalRoot . '/sites/default/services.yml')
      && $fs->exists($drupalRoot . '/sites/default/default.services.yml')) {
      $fs->copy(
        $drupalRoot . '/sites/default/default.services.yml',
        $drupalRoot . '/sites/default/services.yml'
      );
      $event->getIO()->write('Create a sites/default/default.services.yml file');
    }

    // Create the files directory with chmod 0775
    if (!$fs->exists($drupalRoot . '/sites/default/files')) {
      $oldmask = umask(0);
      $fs->mkdir($drupalRoot . '/sites/default/files', 0775);
      umask($oldmask);
      $event->getIO()->write("Create a sites/default/files directory with chmod 0775");
    }
  }
}
