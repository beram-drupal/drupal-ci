#!/bin/sh

set -eu

usage() {
    printf "\n"
    printf "Usage:\n\t%s\n" "$0"
    printf "\n"
    exit 0
}

if [ "${1+x}" = '-h' ]; then
    usage
fi

cd "$( dirname "$0" )"/..

./scripts/manage-tests-module.sh --create
./bin/phpunit
./scripts/manage-tests-module.sh --remove
