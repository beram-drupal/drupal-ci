#!/bin/sh

set -eu

usage() {
    printf "\n"
    printf "Usage:\n\t%s --create|--remove\n" "$0"
    printf "\n"
    printf "Options:\n"
    printf "\t--create : Create the tests module.\n"
    printf "\t--remove : Remove the tests module.\n"
    printf "\n"
    exit 0
}

CREATE=false
REMOVE=false;

case "$1" in
    -h)
        usage
        ;;
    --create)
        CREATE=true
        ;;
    --remove)
        REMOVE=true
        ;;
esac

if [ "$CREATE" = false ] && [ "$REMOVE" = false ]; then
   echo  "Error: Please either use the --create or the --remove option."
   exit 1
fi

cd "$( dirname "$0" )"/..

TESTS_MODULE_ROOT_DIR="./app/modules/custom/drupalci/drupalci_tests"
TESTS_MODULE_CONFIG_DIR="$TESTS_MODULE_ROOT_DIR/config/optional"

remove_tests_module() {
    if [ -d "$TESTS_MODULE_ROOT_DIR" ]; then
        rm -r "$TESTS_MODULE_ROOT_DIR"
    fi
}

create_tests_module() {
    remove_tests_module

    printf "> Create the tests module.\n"

    mkdir -p "$TESTS_MODULE_CONFIG_DIR"

    printf "name: 'DRUPALCI Tests'\ntype: module\ncore: 8.x" > "$TESTS_MODULE_ROOT_DIR/drupalci_tests.info.yml"

    find ./config/sync \
        -maxdepth 1 \
        -type f \
        -name "*.yml" \
        -not -name "core.extension.yml" \
        -not -name "system.site.yml" \
        -exec cp {} "$TESTS_MODULE_CONFIG_DIR" \;
    find "$TESTS_MODULE_CONFIG_DIR" \
        -type f \
        -exec sed -i '1 s/^uuid:.*//' {} \;
}

if [ "$CREATE" = true ]; then
    create_tests_module
fi

if [ "$REMOVE" = true ]; then
    printf "> Remove the tests module.\n"
    remove_tests_module
fi
