# Drupal CI

[![pipeline status](https://gitlab.com/beram-drupal/drupal-ci/badges/master/pipeline.svg)](https://gitlab.com/beram-drupal/drupal-ci/commits/master)

Let's have fun with CI and Drupal!

## Requirements

 * [Docker](https://docs.docker.com/)
 * [Docker Compose](https://docs.docker.com/compose/overview/)
 * [Git](https://git-scm.com/)

## Installation

```bash
$ git clone git@gitlab.com:beram-drupal/drupal-ci.git
$ cd drupal-ci/
$ cp .env.dist .env
$ echo "127.0.0.1 ci-mydrupal.local.docker" | sudo tee -a /etc/hosts
$ make project-install
```

Check available `make` commands:

```bash
$ make help
```

## Documentation

 * [Contributing guide](CONTRIBUTING.md)
 * [Drupal guide](docs/drupal)
 * [Redis guide](docs/redis)
