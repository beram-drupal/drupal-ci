# Translations

You need to keep custom module translation files up to date.
Please every time you add or modify a translatable markup, update the files.

To enable translations support for a custom module add this lines to the `.info.yml`:

```
interface translation project: mymodule
interface translation server pattern: path/to/mymodule/translations/%language.po
```

To import translations you can use:

 * drush locale commands (`drush help | grep locale` to list them)
 * the BO with the `User interface translation` (/admin/config/regional/translate/import)

Note:

 * You can use a context to avoid conflict with other translations: see the [Translation API](https://www.drupal.org/docs/8/api/translation-api/overview)
 * You can also use the module [Drush Language Commands](https://www.drupal.org/project/drush_language)
