# Drush

Check the [documentation](http://docs.drush.org/).

## Work with Drush

Drush 9 is installed on this project.

To work with Drush, run all commands from `php` service container.

```bash
$ make docker-shell-php
```

List all Drush commands available:

```bash
$ drush list
```

For useful and day-to-day commands check the `Makefile`

All available default commands are described on [Drush Commands](https://drushcommands.com/).
