# Redis

For more information please read the [Redis documentation](https://redis.io/documentation).

## Check Redis

Quick and easy check:

 * Enable the `Redis` module if it is not already the case: `drush en redis`
 * Log in as the admin user.
 * Go to `http://${YOUR_DOMAIN}/admin/help/redis`
 * If the message `Current connected client uses the PhpRedis library.` appeared it is OK.

Or for a more precise check:

 * Connect to `redis` container:

```bash
$ make docker-shell-redis
```

 * Check ping response:
 
```bash
$ redis-cli ping
PONG
```

 * Get a list of keys:

```bash
$ redis-cli --scan | head -10
mydrupal_:default:twig:5b5cd29061fbe_user.html.twig_KTRNr226v0rq8yWMg4jpuD1r8
mydrupal_:discovery:entity_bundle_field_definitions:block_content:basic:en
mydrupal_:default:twig:5b5cd29061fbe_menu-local-tasks.html.twi_-U9q0KfMoCml9W-9XJZZCwICm
mydrupal_:data:views:unpack_options:5f17b21c8d2359d3d4c5f5c37b4ea93fe5d461974d5b94d3d83eb4b0aa167bc8:en
mydrupal_:default:twig:5b5cd29061fbe_region--header.html.twig_lzq1A9ghT2jjveN_i8p4TyuUy
mydrupal_:default:views_data:taxonomy_term_data:en
mydrupal_:data:route_provider.route_load:584a0a1b577a4bc9186ed9f8803085cfbd1a79514247bbc9a71753ad7f5b5701aa10a73a9d2e4b5f13eabd61522fcd55b844c9c62b5617d8def08b3c97a43b91
mydrupal_:config:field.field.comment.comment.comment_body
mydrupal_:data:route:en:/user/login:
mydrupal_:data:route_provider.route_load:8d864ede97139ae8661122133f970c5421a9b1d5ff6a2a1c99df6501d635bf5abafd8776b0be67418cc6fb8949b8a6b1eb947caa5d7330ebae00b5f7bf03ecea
```

## Continuous stats mode

It is a very useful feature of `redis-cli` in order to monitor Redis instances in real time.
To enable this mode, the `--stat` option is used.
The output is very clear about the behavior of the CLI in this mode:

```bash
$ redis-cli --stat
------- data ------ --------------------- load -------------------- - child -
keys       mem      clients blocked requests            connections
506        1015.00K 1       0       24 (+0)             7
506        1015.00K 1       0       25 (+1)             7
506        3.40M    51      0       60461 (+60436)      57
506        3.40M    51      0       146425 (+85964)     107
507        3.40M    51      0       233844 (+87419)     157
507        3.40M    51      0       321715 (+87871)     207
508        3.40M    51      0       408642 (+86927)     257
508        3.40M    51      0       497038 (+88396)     257
```
