<?php

if ($GLOBALS['__DRUPALCI_XDEBUG_IS_ENABLED'] = \extension_loaded('xdebug')) {
  \xdebug_set_filter(XDEBUG_FILTER_CODE_COVERAGE, XDEBUG_PATH_WHITELIST, [
      realpath(sprintf('%s/app/modules/custom/drupalci/drupalci_core/', __DIR__))
    ]
  );
}

require sprintf('%s/app/core/tests/bootstrap.php', __DIR__);
